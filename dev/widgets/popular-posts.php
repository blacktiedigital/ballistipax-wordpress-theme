<div class="widget widget--pop-posts">
    <h2>
        Most Popular
    </h2>
    <ul>
        <?php
        $popularpost = new WP_Query( array( 'posts_per_page' => 7, 'meta_key' => 'black_tie_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
        while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
            <li>
                <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a>
            </li>
        <?php endwhile; ?>
    </ul>
</div>
