<?php /** Standard Post Feed **/ get_template_part('header'); ?>

<!-- Global Wrapper -->
<div class="wrapper wrapper--feed">

    <!-- Post Feed -->
    <section class="wrapper__inner">

        <!-- Begin Post Loop -->
        <?php if(have_posts()): while(have_posts()): the_post(); ?>

            <!-- Individual Post -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(['post', 'post--blog']); ?> role="article">

                <!-- Post Featured Image -->
                <figure>
                    <a href="<?php the_permalink(); ?>" class="post__image">
                        <?php if(get_the_post_thumbnail_url()): ?>
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_caption() ? get_the_post_thumbnail_caption() : get_the_title(); ?>" />
                        <?php endif; ?>
                    </a>

                    <!-- Post Date -->
                    <p class="post__date">

                        <!-- Month -->
                        <span class="date--month">
                            <?php the_time('M'); ?>
                        </span>

                        <!-- Day -->
                        <span class="date--day">
                            <?php the_time('d'); ?>
                        </span>

                        <!-- Year -->
                        <span class="date--year">
                            <?php the_time('Y'); ?>
                        </span>
                    </p>
                </figure>

                <!-- Post Categories -->
                <?php
                $id = get_the_ID();
                $cats = get_the_terms($id, 'category');
                if (count($cats) > 0): ?>
                    <p class="post__categories">
                        Posted in:
                        <?php foreach ( $cats as $cat ): ?>
                            <a href="<?php echo get_category_link($cat); ?>" class="term">
                                <?php echo $cat->name; ?>
                            </a>
                        <?php endforeach; ?>
                    </p>
                <?php endif; ?>

                <!-- Post Title -->
                <h2 class="post__title">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>
                </h2>
            </article>
        <?php endwhile;
            wp_reset_query();
        endif; ?>
    </section>

    <!-- Sidebar -->
    <div class="sidebar">
        <?php get_template_part('sidebar'); ?>
    </div>

    <!-- Pagination -->
    <nav class="post__pagination">
        <?php $paginateArgs = array(
            'base'                  => @add_query_arg('paged','%#%'),
            'format'                => '?paged=%#%',
            'total'                 => $wp_query->max_num_pages,
            'current'               => max(1, get_query_var('paged')),
            'show_all'              => false,
            'end_size'              => 1,
            'mid_size'              => 2,
            'prev_next'             => false,
            'prev_text'             => __(''),
            'next_text'             => __(''),
            'type'                  => 'plain',
            'add_args'              => false,
            'add_fragment'          => '',
            'before_page_number'    => '',
            'after_page_number'     => ''
        ); echo paginate_links($paginateArgs); ?>
    </nav>
</div>
<?php get_template_part('footer'); ?>
