<?php /** Single Post **/ get_template_part('header'); ?>

<!-- Global Wrapper -->
<div class="wrapper wrapper--post">
    <?php if(have_posts()) : while(have_posts()) : the_post();

        // Count a view towards ranking for popular posts widget
        black_tie_set_post_views(get_the_ID()); ?>

        <!-- Post --->
        <article id="post-<?php the_ID(); ?>" <?php post_class(['post', 'post--single']); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

            <!-- Post Heading --->
            <header class="header header--post u-relative_hidden">

                <!-- Post Featured Image --->
                <figure class="u-full_cover_absolute">
                    <a href="<?php the_permalink(); ?>" class="post__image u-full_cover_absolute">
                        <?php if(get_the_post_thumbnail_url()): ?>
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_post_thumbnail_caption() ? get_the_post_thumbnail_caption() : get_the_title(); ?>" />
                        <?php endif; ?>
                    </a>
                </figure>

                <h1>
                    <?php the_title(); ?>
                </h1>
            </header>

            <div class="post__inner">
                <?php the_content(); ?>
            </div>
        </article>

        <?php if(is_paged()) : ?>
            <nav class="pagination">
                <div class="pagination__prev pagination__link">
                    <?php echo get_next_posts_link('&laquo; Older Entries') ?>
                </div>
                <div class="pagination__next pagination__link">
                    <?php echo previous_posts_link('Newer Entries &raquo;') ?>
                </div>
            </nav>
        <?php endif;
    endwhile;
    endif; ?>
</div>

<?php get_template_part('footer'); ?>
