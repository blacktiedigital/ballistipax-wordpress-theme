const
    menuMain = document.querySelector('.menu--main'),
    menuFooter = document.querySelector('.menu--footer'),
    header = document.querySelector('.header--global'),
    footer = document.querySelector('.footer--global'),
    hamburgerMain = header.querySelector('.o-button--burger'),
    menuAux = header.querySelector('.menu--aux');

// Throttles menuResize() for performance
import deBounce from './_debounce';

// NAVIGATION ================================
// ===========================================

// Main menu hamburger
if (hamburgerMain !== null) {
    hamburgerMain.onclick = () => {
        header.classList.toggle('js-menu-active');
        menuMain.classList.toggle('js-active');
        hamburgerMain.classList.toggle('js-active');
    };
}

// Can conditionally control display states for menus
const menuState = (menu, parent, offset, hamburger, sibling) => {
    if (menu) {
        const menuItems = menu.querySelectorAll('a');
        let menuWidth = 0;

        menuItems.forEach(item => {
            menuWidth += item.clientWidth;
        });

        const menuResize = () => {
            const windowWidth = window.innerWidth;
            if (menuWidth + offset <= windowWidth) {

                // Toggle Menu Classes
                menu.classList.add('js-desktop');
                menu.classList.remove('js-mobile');
                menu.classList.remove('js-active');

                // Toggle Desktop Classes
                parent.classList.add('js-desktop');
                parent.classList.remove('js-mobile');
                parent.classList.remove('js-menu-active');

                // Toggle Hamburger Classes
                hamburger && hamburger.classList.add('js-desktop');
                hamburger && hamburger.classList.remove('js-mobile');
                hamburger && hamburger.classList.remove('js-active');

                // Sibling Menu
                sibling && sibling.classList.add('js-desktop');
                sibling && sibling.classList.remove('js-mobile');
                sibling && sibling.classList.remove('js-active');
            } else {

                // Toggle Menu Classes
                menu.classList.add('js-mobile');
                menu.classList.remove('js-desktop');

                // Toggle Desktop Classes
                parent.classList.remove('js-desktop');
                parent.classList.add('js-mobile');

                // Toggle Hamburger Classes
                hamburger && hamburger.classList.remove('js-desktop');
                hamburger && hamburger.classList.add('js-mobile');

                // Sibling Menu
                sibling && sibling.classList.remove('js-desktop');
                sibling && sibling.classList.add('js-mobile');
            }
        };

        menuResize();
        const checkMenuWidth = deBounce(() => {
            menuResize();
        }, 50);
        window.addEventListener('resize', checkMenuWidth);
    }
};

// Main Menu
menuState(menuMain, header, 400, hamburgerMain, menuAux);

// Footer Menu
menuState(menuFooter, footer, 100);
