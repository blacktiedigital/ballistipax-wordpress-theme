import deBounce from './_debounce';
import jump from 'jump.js'

const
    pageAnchors = document.querySelectorAll('a'),
    anchorAdjust = () => {
        jump(-60);
    },
    checkHash = () => {
        if (window.location.hash) {
            if (document.readyState === "complete") {
                anchorAdjust();
            } else {
                setTimeout(() => {
                    checkHash();
                }, 100);
            }
        }
    };

document.addEventListener('DOMContentLoaded', function() {
    checkHash();
});

pageAnchors.forEach((anchor) => {
    anchor.addEventListener('click', function() {
        setTimeout(() => {
            anchorAdjust();
        }, 10);
    })
});

// Adds an active class to the
// header / menu if the user scrolls down
const windowScroll = () => {
    let scrollTop = window.pageYOffset;
    const
        header = document.querySelector('.header--global'),
        mainMenu = document.querySelector('.menu--main'),
        hamburgerMain = header.querySelector('.o-button--burger');

    if (scrollTop > 0) {
        header && header.classList.add('js-active');
        mainMenu && mainMenu.classList.add('js-header-active');
        hamburgerMain && hamburgerMain.classList.add('js-header-active');
    } else {
        header && header.classList.remove('js-active');
        mainMenu && mainMenu.classList.remove('js-header-active');
        hamburgerMain && hamburgerMain.classList.remove('js-header-active');
    }
};

windowScroll();
const checkMenuWidth = deBounce(() => {
    windowScroll();
}, 10);
window.addEventListener('scroll', checkMenuWidth);
