<?php
include_once('config/config.php');
if (!function_exists('black_tie_theme_init')) {
    function black_tie_theme_init()
    {
        // Enqueue scripts
        add_action('wp_enqueue_scripts', 'black_tie_theme_assets');
        // Register menus
        black_tie_register_menus();
        // Add thumbnail support and set widths
        black_tie_post_thumbnail_support();
        // Link post thumbnail to post permalink
        add_filter('post_thumbnail_html', 'black_tie_post_image_html', 10, 3);
        // Remove width/height attribute on inserted images
        add_filter('post_thumbnail_html', 'black_tie_remove_width_attribute', 10);
        // Images are sized via CSS or inline style="" attribute by user.
        add_filter('image_send_to_editor', 'black_tie_remove_width_attribute', 10);
        // Enable custom menus
        add_theme_support('menus');
        // Automatic feeds
        add_theme_support('automatic-feed-links');
        // SVG uploads to admin
        add_filter('upload_mimes', 'black_tie_mime_types');
        // Popular Posts Counter
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
        // Tracks visit count on posts for 'most popular' widget
        add_action('wp_head', 'black_tie_track_post_views');
        // Exclude pages from search
        add_action( 'init', 'exclude_pages', 99 );
        // Login Form Stylesheet
        add_action( 'login_enqueue_scripts', 'black_tie_login_stylesheet' );
        // Black tie email domain preference
        add_action('register_post', 'btd_is_valid_email_domain', 10, 3);
        // REST Fields
        add_action('rest_api_init', 'black_tie_add_custom_rest_fields');
        // Enqueue theme assets
        add_action('wp_enqueue_scripts', 'black_tie_theme_assets');
        // Enqueue block editor assets
        add_action('enqueue_block_editor_assets', 'black_tie_editor_theme_assets');
        // Defer all script tags
        add_filter('script_loader_tag', 'black_tie_defer_js', 10);
        // Woo validation
        add_action('after_setup_theme', 'black_tie_add_woocommerce_support');
    }
}

// thumbs up, lets go!
black_tie_theme_init();
