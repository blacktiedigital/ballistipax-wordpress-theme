<?php /* Default Page */ get_template_part('header'); ?>

<div class="wrapper wrapper--page">
    <?php while(have_posts()): the_post();
        the_content();
    endwhile; ?>
</div>

<?php get_template_part('footer'); ?>
