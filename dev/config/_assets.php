<?php

// Theme styles
if (!function_exists('black_tie_theme_assets')) {
    function black_tie_theme_assets()
    {
        // Universal Styles
        wp_enqueue_style('google_pre_connect', 'https://fonts.gstatic.com', false, null);
        wp_enqueue_style('google_fonts', 'https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;700&display=swap', false, null);
        wp_enqueue_style('styles', get_template_directory_uri() . '/css/theme.css', false, null);

        // Site scripts
        wp_enqueue_script('internal_build_js', get_template_directory_uri() . '/js/theme.min.js', '', null, true);
    }
}

// Gutenberg editor assets
if (!function_exists('black_tie_editor_theme_assets')) {
    function black_tie_editor_theme_assets()
    {
        // MUST include font link here as well for editor consistency
        wp_enqueue_style('google_fonts', 'https://fonts.googleapis.com/css2?family=Rajdhani:wght@400;700&display=swap', false, null);
        wp_enqueue_style('black-tie-block-style', get_template_directory_uri() . '/css/blocks.css', '');
    }
}

// Defer all script tags
if (!function_exists('black_tie_defer_js')) {
    function black_tie_defer_js($url)
    {
        if (is_admin()) return $url;
        if (FALSE === strpos($url, '.js')) return $url;
        if (strpos($url, 'jquery.js')) return $url;
        return str_replace(' src', ' defer src', $url);
    }
}
