<?php
// Remove the inline width/height attributes on images, because it's not 2003
if (!function_exists('black_tie_remove_width_attribute')) {
    function black_tie_remove_width_attribute($html)
    {
        return preg_replace('/(width|height)="\d*"\s/', "", $html);
    }
}

// Allow SVGs in Media Uploader
if(!function_exists('black_tie_mime_types')) {
    function black_tie_mime_types($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
}
