<?php
// Post Thumbnail Support
if (!function_exists('black_tie_post_thumbnail_support')) {
    function black_tie_post_thumbnail_support()
    {
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1200, 600);   // Uncomment to add post thumbnail sizes
    }
}

// Link post thumbnail to post permalink
if (!function_exists('black_tie_post_image_html')) {
    function black_tie_post_image_html($html, $post_id, $post_image_id)
    {
        return '<a href="' . get_permalink($post_id) . '" title="' .
            esc_attr(get_post_field('post_title', $post_id)) . '">' . $html . '</a>';
    }
}
