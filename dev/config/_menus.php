<?php
if(!function_exists('black_tie_register_menus')) {
    function black_tie_register_menus() {
        register_nav_menus(array(
            'primary_navigation' => __('Primary Navigation', 'btd-theme'),
            'footer_navigation' => __('Footer Navigation', 'btd-theme'),
            'footer_navigation_secondary' => __('Footer Navigation Secondary', 'btd-theme'),
        ));
    }
}
