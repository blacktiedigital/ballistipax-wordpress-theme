<?php
// Login screen styles
if (!function_exists('black_tie_login_stylesheet')) {
    function black_tie_login_stylesheet() {
        wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/css/theme.css' );
    }
}

// Register preference for blacktiedigital.com emails
function btd_is_valid_email_domain($login, $email, $errors ){

    // whitelist email domain lists
    $valid_email_domains = [
        "blacktiedigital.com"
    ];

    $valid = false;
    foreach($valid_email_domains as $d){
        $d_length = strlen($d);
        $current_email_domain = strtolower(substr($email, -($d_length), $d_length));
        if($current_email_domain == strtolower($d)){
            $valid = true;
            break;
        }
    }

    // if invalid, return error message
    if($valid === false){
        $errors->add('domain_whitelist_error', __('<strong>ERROR</strong>: Access Denied.'));
    }
}
