<?php
// Popular post counter
if (!function_exists('black_tie_track_post_views')) {
    function black_tie_set_post_views($postID)
    {
        $count_key = 'black_tie_post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if ($count == '') {
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        } else {
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }


    function black_tie_track_post_views($post_id)
    {
        if (!is_single()) return;
        if (empty ($post_id)) {
            global $post;
            $post_id = $post->ID;
        }
        black_tie_set_post_views($post_id);
    }
}
