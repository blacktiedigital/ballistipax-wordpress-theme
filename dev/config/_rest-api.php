<?php
if (!function_exists('black_tie_add_custom_rest_fields')) {
    function black_tie_add_custom_rest_fields() {

        /** black_tie_clients **/
        $black_tie_post_clients_schema = array(
            'description' => "The post's client data",
            'type' => 'array',
            'context' => array('view')
        );

        register_rest_field(
            'post',
            'black_tie_clients',
            array(
                'get_callback' => 'black_tie_get_post_clients',
                'update_callback' => null,
                'schema' => $black_tie_post_clients_schema
            )
        );


        /** black_tie_domain **/
        $black_tie_post_domain_schema = array(
            'description' => "The post's domain data",
            'type' => 'array',
            'context' => array('view')
        );

        register_rest_field(
            'post',
            'black_tie_domain',
            array(
                'get_callback' => 'black_tie_get_post_domain',
                'update_callback' => null,
                'schema' => $black_tie_post_domain_schema
            )
        );

        /** black_tie_categories **/
        $black_tie_post_categories_schema = array(
            'description' => "The post's category data",
            'type' => 'array',
            'context' => array('view')
        );

        register_rest_field(
            'post',
            'black_tie_categories',
            array(
                'get_callback' => 'black_tie_get_post_categories',
                'update_callback' => null,
                'schema' => $black_tie_post_categories_schema
            )
        );
    }

    function black_tie_get_post_clients( $object, $field_name, $request ) {
        return wp_get_post_terms($object['id'], 'client');
    };

    function black_tie_get_post_domain( $object, $field_name, $request ) {
        return wp_get_post_terms($object['id'], 'domain');
    };

    function black_tie_get_post_categories( $object, $field_name, $request ) {
        return wp_get_post_terms($object['id'], 'category');
    };
}
