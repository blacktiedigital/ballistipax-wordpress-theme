<?php
$themeColors = [
    [
        'name' => __('Ballistipax Grey', 'black-tie'),
        'slug' => 'btd-primary',
        'color' => '#6d6d6d',
    ],
    [
        'name' => __('Darkest Grey', 'black-tie'),
        'slug' => 'btd-darkest-grey',
        'color' => '#101010',
    ],
    [
        'name' => __('Darker Grey', 'black-tie'),
        'slug' => 'btd-darker-grey',
        'color' => '#202020',
    ],
    [
        'name' => __('Dark Grey', 'black-tie'),
        'slug' => 'btd-dark-grey',
        'color' => '#404040',
    ],
    [
        'name' => __('Mid Grey', 'black-tie'),
        'slug' => 'btd-mid-grey',
        'color' => '#808080',
    ],
    [
        'name' => __('Light Grey', 'black-tie'),
        'slug' => 'btd-light-grey',
        'color' => '#bfbfbf',
    ],
    [
        'name' => __('Lighter Grey', 'black-tie'),
        'slug' => 'btd-lighter-grey',
        'color' => '#e6e6e6',
    ],
    [
        'name' => __('Lightest Grey', 'black-tie'),
        'slug' => 'btd-lightest-grey',
        'color' => '#f0f0f0',
    ],
    [
        'name' => __('White', 'black-tie'),
        'slug' => 'btd-white',
        'color' => '#fff',
    ],
    [
        'name' => __('Black', 'black-tie'),
        'slug' => 'btd-black',
        'color' => '#000',
    ],
];
add_theme_support( 'editor-color-palette', $themeColors );
