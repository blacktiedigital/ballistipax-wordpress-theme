<?php
// Remove woo lack of theme support
if (!function_exists('black_tie_add_woocommerce_support')) {
    function black_tie_add_woocommerce_support() {
        add_theme_support('woocommerce');
    }
}
