<?php
// Exclude pages from search results
if (!function_exists('exclude_pages')) {
    function exclude_pages() {
        global $wp_post_types;

        if ( post_type_exists( 'page' ) ) {
            // exclude from search results
            $wp_post_types['page']->exclude_from_search = true;
        }
    }
}
