<?php
/**
 * Woocommerce compliance:
 * @version 3.4.0
 */

// Assigns the query object to a handy variable
global $wp_query;

// Create two empty vars that will be
// populated based on the taxonomy used
$query_type = null;
$query_title = null;

// Supports product categories, product class,
// weapon and product tag taxonomies
switch ($wp_query->query) {

    // Product Category
    case $wp_query->query["product_cat"] !== null:
        $query_type = 'product_cat';
        $query_title = $wp_query->query["product_cat"];
        break;

    // Product Tag
    case $wp_query->query["product_tag"] !== null:
        $query_type = 'product_tag';
        $query_title = $wp_query->query["product_tag"];
        break;

    // Default condition
    default:
        $query_title = "unknown query";
} get_template_part('header');
?>

<div class="wrapper wrapper--collection">

    <!--<div class="collection__description">

    </div>-->

    <div class="collection__loop">
        <h1>
            <?php echo implode('-', array_map('ucfirst', explode('-', $query_title)));; ?>
        </h1>

<!--        <div class="btd-breadcrumb btd-breadcrumb--collections">-->
<!---->
<!--        </div>-->

        <div class="collection__loop-inner">
            <?php
            if ($wp_query->have_posts()): while ($wp_query->have_posts()) : $wp_query->the_post();
                global $product;

                $largeImage = wp_get_attachment_image(intval($product->image_id), 'woocommerce_single');
                $fullImage = wp_get_attachment_image(intval($product->image_id), 'full');
                $priceCondition = $product->sale_price !== '';
                $finalPrice = $priceCondition ? $product->sale_price : $product->regular_price; ?>

                <article id="product-<?php the_ID(); ?>" class="product" role="article">
                    <figure>
                        <a href="<?php the_permalink(); ?>" class="u-full_cover_absolute">
                            <?php echo $largeImage !== "" ? $largeImage : $fullImage ?>
                        </a>

                    </figure>

                    <div class="product__info">
                        <h3 class="product__title"><?php the_title(); ?></h3>
                        <p class="product__price <?php $priceCondition && 'is-on-sale' ?>">$<?php echo $finalPrice; ?></p>
                    </div>
                </article>
            <?php endwhile;
                $paginateArgs = array(
                    'base'                  => @add_query_arg('paged','%#%'),
                    'format'                => '?paged=%#%',
                    'total'                 => $wp_query->max_num_pages,
                    'current'               => max(1, get_query_var('paged')),
                    'show_all'              => false,
                    'end_size'              => 1,
                    'mid_size'              => 2,
                    'prev_next'             => false,
                    'prev_text'             => __(''),
                    'next_text'             => __(''),
                    'type'                  => 'plain',
                    'add_args'              => false,
                    'add_fragment'          => '',
                    'before_page_number'    => '',
                    'after_page_number'     => ''
                ); ?>

                <div class="collections__pagination">
                    <?php echo paginate_links($paginateArgs); ?>
                </div>
            <?php endif; wp_reset_postdata(); ?>
        </div>
    </div>
</div>

<?php get_template_part('footer'); ?>
