<!doctype html>
<!--
    ██████╗ ████████╗██████╗     ██████╗  ██████╗ ██████╗  ██╗
    ██╔══██╗╚══██╔══╝██╔══██╗    ╚════██╗██╔═████╗╚════██╗███║
    ██████╔╝   ██║   ██║  ██║     █████╔╝██║██╔██║ █████╔╝╚██║
    ██╔══██╗   ██║   ██║  ██║    ██╔═══╝ ████╔╝██║██╔═══╝  ██║
    ██████╔╝   ██║   ██████╔╝    ███████╗╚██████╔╝███████╗ ██║
    ╚═════╝    ╚═╝   ╚═════╝     ╚══════╝ ╚═════╝ ╚══════╝ ╚═╝
-->
<html class="no-js" lang="en">
<head>

    <!-- METADATA -->
    <title><?php wp_title(''); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Match w/ header bg color -->
    <meta name="theme-color" content="#000000" />

    <?php wp_head(); ?>

    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
</head>
