#BTD Boilerplate v4.10.4

###Current Issues:
webpack-livereload-plugin does not currently support webpack version 5,
so, we're currently using a altered version from: @kooneko/livereload-webpack-plugin.

No adverse effects have been observed, but I will continue to look out for the proper package to be updated. In the meantime, this alternate version doesn't require any downloading and copy / pasting.

###What's in the box:
* A WordPress theme
* SCSS / JSNEXT
* Autoprefixer for compatibility
* Live reload for the front-end
* Webpack for tasks

###Getting Started
1. Clone this repository
2. In your terminal, navigate to the theme's root directory
3. Node v10 or greater required
4. NPM v6 or greater required
5. Run this command: `npm i` to install dependencies.
6. Run `npm run dev` for development.
7. Run `npm run prod` for production.

###General Notes
* The "dev" and "prod" build tasks both output to the same /build/ folder. *Always activate the /build/ folder in wordpress, never active /dev/*.
* No support for IE:11.
* In some places, you'll find calls to ACF fields that will require the plugin and appropriate field data to be installed.
