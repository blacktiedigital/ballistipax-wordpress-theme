const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const DIST_DIR = path.resolve(__dirname, '../build');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

// Configuration for the ExtractTextPlugin.
const extractConfig = [
    {
        loader: 'file-loader',
        options: {
            name: '[name].css',
            context: './',
            outputPath: '/css',
        }
    },
    {
        loader: 'extract-loader'
    },
    {
        loader: 'css-loader',
    },
    {
        loader: 'sass-loader',
    }
];

module.exports = {
    entry: {
        theme: ['./dev/scripts/theme.js', './dev/styles/theme.scss'],
        blocks: ['./dev/styles/blocks.scss'],
    },

    output: {
        path: DIST_DIR,
        filename: './js/[name].min.js',
    },

    resolve: {
        fallback: {
            util: require.resolve("util/"),
            stream: require.resolve("stream-browserify"),
            buffer: require.resolve("buffer/")
        }
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /theme\.s?css$/,
                exclude: /node_modules/,
                use: extractConfig,
            },
            {
                test: /blocks\.s?css$/,
                exclude: /node_modules/,
                use: extractConfig,
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'url-loader',
                }]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                    },
                ],
            }
        ],
    },

    plugins: [
        new ProgressBarPlugin(),
        new CleanWebpackPlugin(),
        new CopyPlugin({
            patterns: [{
                from: './dev',
                to: './',
                globOptions: {
                    ignore: [
                        "**/scripts/**",
                        '**/styles/**',
                    ]
                }
            }],
        })
    ],
};
